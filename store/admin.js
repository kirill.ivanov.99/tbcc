export const state = () => ({
  category: [],
  articles: []
})

export const mutations  = {
  setCategory(state, value) {
    state.category.push(value);
  },

  setArticle(state, value) {
    state.articles.push(value);
  },
}

export const actions = {
  setCategory(context, value) {
    context.commit('setCategory', value);
  },
  setArticle(context, value) {
    context.commit('setArticle', value);
  }
}

export const getters = {
  getCategory: state => state.category,
  getArticles: state => state.articles,
}
