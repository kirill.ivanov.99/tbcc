export const state = () => ({
  price: 0.0835,
  percentChange: 4.01,
  vol24h: '2344802',
  highPrice:  0.0885,
  lowPrice: 0.0800,
  mktCap: '83140867',
  showFeedbackForm: false,
  showSignInForm: false,
  currentPlan: ''
})

export const mutations  = {
  setPrice(state, value) {
    state.price = value;
  },
  setPercentChange(state, value) {
    state.percentChange = value;
  },
  setVol24h(state, value) {
    state.vol24h = value;
  },
  setHighPrice(state, value) {
    state.highPrice = value;
  },
  setLowPrice(state, value) {
    state.lowPrice = value;
  },
  setMktCap(state, value) {
    state.mktCap = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  toggleShowFeedbackForm(state, value) {
    state.showFeedbackForm = value
  },
  toggleShowSignInForm(state, value) {
    state.showSignInForm = value
  },
  setCurrentPlan(state, value) {
    state.currentPlan = value
  }
}

export const actions = {
  setPrice(context, value) {
    context.commit('setPrice', value);
  },
  setPercentChange(context, value) {
    context.commit('setPercentChange', value);
  },
  setVol24h(context, value) {
    context.commit('setVol24h', value);
  },
  setHighPrice(context, value) {
    context.commit('setHighPrice', value);
  },
  setLowPrice(context, value) {
    context.commit('setLowPrice', value);
  },
  setMktCap(context, value) {
    context.commit('setMktCap', value);
  },
  toggleShowFeedbackForm(context, value) {
    context.commit('toggleShowFeedbackForm', value)
  },
  toggleShowSignInForm(context, value) {
    context.commit('toggleShowSignInForm', value)
  },
  setCurrentPlan(context, value) {
    context.commit('setCurrentPlan', value)
  }
}

export const getters = {
  getPrice: state => state.price,
  getPercentChange: state => state.percentChange,
  getVol24h: state => state.vol24h,
  getHighPrice: state => state.highPrice,
  getLowPrice: state => state.lowPrice,
  getMktCap: state => state.mktCap,
  getFeedbackForm: state => state.showFeedbackForm,
  getSignInForm: state => state.showSignInForm,
  getCurrentPlan: state => state.currentPlan
}
