import Vue from 'vue';
import VueGtag from 'vue-gtag';

export default ({ app }) => {
  Vue.use(VueGtag, {
    config: { id: 'G-RWR3S3CEL5' },
    appName: 'TBCC',
  }, app.router);
}
