import webpack from 'webpack'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TBCC',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'description', content: 'tbcc' }
    ],
    script: [
      {
        src: 'https://smtpjs.com/v3/smtp.js'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/normalize.css',
    '@/assets/css/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/vue-flickity.js', ssr: false },
    { src: '@/plugins/vue-quill-editor', ssr: false },
    { src: '@/plugins/vue-gtag.js', mode: 'client' },
    { src: '@/plugins/vue-load-script.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/i18n',
    '@nuxtjs/google-analytics',
    '@nuxtjs/robots',
    ['nuxt-mail', {
      message: {
        to: 'kir.ivanov99@mail.ru',
      },
      smtp: {
        host : "smtp.mailtrap.io",
        port: 587,
        auth: {
          user: 'bc95ac626774c8',
          pass: 'a6b0636c1a8d36',
        },
      },
    }],
  ],

  googleAnalytics: {
    id: 'G-RWR3S3CEL5'
  },

  i18n: {
    strategy: 'prefix_except_default',
    defaultLocale: 'en',
    locales: [  'ch', 'tur', 'en'],
    vueI18nLoader: true
  },

  axios: {
    proxy: true,
    proxyHeaders: false,
    credentials: false
  },

  robots: {
    UserAgent: '*',
    Disallow: ''
  },

  proxy: {
    '/api/': { target: 'https://www.tbcc.com/', pathRewrite: {'^/api/': ''} },
    '/api1/': { target: 'https://pro-api.coinmarketcap.com/', pathRewrite: {'^/api1/': ''} }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    plugins: [ new webpack.ProvidePlugin({ 'window.Quill': 'quill/dist/quill.js', 'Quill': 'quill/dist/quill.js' }) ]
  }
}
